const webp=require('webp-converter');
const fs = require('fs');

// this will grant 755 permission to webp executables
webp.grant_permission();



async function convertFiles(path) {
  const dir = await fs.promises.opendir(path)
  const srcDir = './src/';
  const distDir = './dist/';

  for await (const image of dir) {
    const imageName = image.name.replace(/\.[^/.]+$/, "");
    const result = webp.cwebp(srcDir + image.name, distDir + imageName + ".webp","-q 80",logging="-v");
    result.then((response) => {
      console.log(response);
    });
  }
}

convertFiles('./src').catch(console.error)
